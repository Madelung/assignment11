package org.example.ui;

//import org.example.domain.Quote;
//import org.example.domain.QuoteRepository;
import org.example.usecase.AddQuote;
import org.example.usecase.AddQuoteHandler;
import org.example.usecase.FindRandomQuote;
import org.example.usecase.FindRandomQuoteHandler;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class AppController implements AddQuoteHandler, FindRandomQuoteHandler {
	
	@FXML private Label statusLabel;
	@FXML private TextArea quoteArea;
	@FXML private Button randomButton;
	@FXML private TextField quoteField;
	@FXML private TextField authorField;
	@FXML private Button addButton;
	
	private final AddQuote addQuote;
	private final FindRandomQuote findRandomQuote;
//	private final QuoteRepository repository;

	public AppController(AddQuote addQuote, FindRandomQuote findRandomQuote) {
		this.addQuote = addQuote;
		this.findRandomQuote = findRandomQuote;
//		this.repository = repository;
	}
	
	@FXML
	private void initialize() {
		randomButton.setOnAction(event -> findRandomQuote.find(this));
		
		addButton.setOnAction(event -> {
//			repository.add(quoteField.getText(), authorField.getText());
//			statusLabel.setText(repository.getSize() + " quotes in collection");
			authorField.clear();
			quoteField.clear();
		});
		
//		statusLabel.setText(repository.getSize() + " quotes in collection");
	}

	@Override
	public void foundQuote(String quote, String author) {
		quoteArea.setText("\"" + quote + "\" - " + author);
	}

	@Override
	public void addedQuote(int total) {
		statusLabel.setText(total + " quotes in collection");
	}
	
	public void updateQuote(String quote, String author) {
		//TextArea textArea = (TextArea) stage.getScene().lookup("#quoteArea");
		quoteField.setText("\"" + quote + "\" - " + author);
	}
}
