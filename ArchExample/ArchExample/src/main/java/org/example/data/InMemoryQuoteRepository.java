package org.example.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.example.domain.Quote;
import org.example.domain.QuoteRepository;

//import javafx.scene.control.TextField;

public class InMemoryQuoteRepository implements QuoteRepository {

	private final List<Quote> quotes = new ArrayList<>();
	
	@Override
	public void add(Quote quote) {
		quotes.add(quote);
	}

	@Override
	public Quote getRandom() {
		Random r = new Random();
		int index = r.nextInt(quotes.size());
		return quotes.get(index);
	}

	@Override
	public int getSize() {
		return quotes.size();
	}

	@Override
	public void add(String quote, String author) {
		Quote q = new Quote(author, quote);
		quotes.add(q);
	}
}
