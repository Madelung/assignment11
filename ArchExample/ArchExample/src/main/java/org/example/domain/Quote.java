package org.example.domain;

public class Quote {
	
	private final String author;
	private final String text;

	public Quote(String author, String text) {
		this.author = author;
		this.text = text;
	}
	
	public String getAuthor() {
		return author;
	}

	public String getText() {
		return text;
	}
}
