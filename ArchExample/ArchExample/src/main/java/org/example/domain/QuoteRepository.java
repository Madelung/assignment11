package org.example.domain;

public interface QuoteRepository {
	
	void add(Quote quote);
	
	void add(String quote, String author);
	
	Quote getRandom();
	
	int getSize();
}
