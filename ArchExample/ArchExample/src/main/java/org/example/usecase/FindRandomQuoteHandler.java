package org.example.usecase;

public interface FindRandomQuoteHandler {
	
	void foundQuote(String quote, String author);
}
