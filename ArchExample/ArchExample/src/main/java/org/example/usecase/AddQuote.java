package org.example.usecase;

//import org.example.data.InMemoryQuoteRepository;
import org.example.domain.Quote;


public class AddQuote {
	
//	private final InMemoryQuoteRepository repository;

	public AddQuote() {
//		this.repository = repository;
	}
	
	public Quote add(String author, String text) {
		Quote quote = new Quote(author, text);
		
//		repository.add(quote);
		
//		handler.addedQuote(repository.getSize());
		
		return quote;
	}
}
