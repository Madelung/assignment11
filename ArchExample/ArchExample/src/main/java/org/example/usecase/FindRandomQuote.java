package org.example.usecase;

import org.example.domain.Quote;
import org.example.domain.QuoteRepository;

public class FindRandomQuote {
	
	private final QuoteRepository repository;

	public FindRandomQuote(QuoteRepository repository) {
		this.repository = repository;
	}
	
	public Quote find(FindRandomQuoteHandler handler) {
		Quote quote = repository.getRandom();
		
		return quote;
	}
}
