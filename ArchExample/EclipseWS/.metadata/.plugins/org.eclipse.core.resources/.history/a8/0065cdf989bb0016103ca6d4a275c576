package org.example;

import org.example.data.InMemoryQuoteRepository;
import org.example.domain.Quote;
import org.example.ui.AppController;
import org.example.usecase.AddQuote;
import org.example.usecase.FindRandomQuote;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application
{
	@Override
	public void start(Stage primaryStage) throws Exception {
		InMemoryQuoteRepository repository = new InMemoryQuoteRepository();
		repository.add(new Quote("Phil Karlton", "There are only two hard things in Computer Science: cache invalidation and naming things."));
		repository.add(new Quote("Grady Booch", "The code is the truth, but it is not the whole truth."));
		repository.add(new Quote("Master Yoda", "Do or do not. There is no try."));
		
		AddQuote addQuote = new AddQuote(repository);
		FindRandomQuote findRandomQuote = new FindRandomQuote(repository, primaryStage);
		
		AppController appController = new AppController(addQuote, findRandomQuote, repository);
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(AppController.class.getResource("App.fxml"));
		loader.setController(appController);
		loader.load();
		
		appController.addedQuote(repository.getSize());
		
		primaryStage.setTitle("Quotes");
		primaryStage.setScene(new Scene(loader.getRoot()));
		primaryStage.show();
	}
	
	public static void main( String[] args )
    {
        Application.launch(App.class, args);
    }
}
